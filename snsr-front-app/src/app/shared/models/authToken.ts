export class AuthToken {
  accessToken: string;
  role: string;
  active: boolean;

  constructor(accessToken: string, role: string, active: boolean) {
    this.accessToken = accessToken;
    this.role = role;
    this.active = active;
  }
}
