import {AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {BaseStationService} from '../../services/base.station.service';
import {SensorService} from '../../services/sensor.service';
import {SensorValue} from '../../models/sensor.value';
import {SensorValueType} from '../../models/sensor.value.type';
import {DatePipe} from '@angular/common';
import {Chart} from 'chart.js';
import * as palette from 'google-palette';

@Component({
  selector: 'app-chart-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})
export class LineComponent implements OnInit, AfterViewInit, OnChanges {

  canvas: any;
  ctx: any;
  chart: Chart;
  @ViewChild('chartRef', {static: true}) chartRef;

  @Input() updatedData: [string, SensorValue[]][];
  @Input() updatedType: SensorValueType;
  @Input() isReady: boolean;

  constructor(private baseStationService: BaseStationService,
              private sensorService: SensorService,
              private datePipe: DatePipe,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initChart();
    this.refresh();
  }

  refresh() {
    if (this.isReady) {
      this.chart.options.title.text = `Зависимость показателя \"${this.updatedType.typeName}\"`;
      this.chart.options.scales.yAxes[0].scaleLabel.labelString = this.updatedType.code;
      this.removeDataSets();
      this.addDataSets(this.updatedData);
    }
  }

  prepareDataSets(data: [string, SensorValue[]][]) {
    const preparedDataSets = [];
    const colors = palette('cb-Set1', data.length).map((hex) => {
        return '#' + hex;
      });
    data.forEach((tuple, index) => {
      const dataSet = tuple[1].map((value) => {
        return {x: Math.round(value.receivedTime), y: value.value};
      });
      preparedDataSets.push(
        {
          data: dataSet,
          borderColor: colors[index],
          label: `\"${tuple[0]}\"`,
          fill: false
        });
    });
    return preparedDataSets;
  }

  initChart() {
    this.canvas = this.chartRef.nativeElement;
    this.ctx = this.canvas.getContext('2d');

    this.chart = new Chart(this.ctx, {
      type: 'line',
      data: {
        datasets: [],
      },
      options: {
        responsive: true,
        // layout: {
        //   padding: {
        //     left: 450,
        //     right: 0,
        //     top: 30,
        //     bottom: 0
        //   }
        // },
        title: {
          display: true,
          text: `Зависимость показателя \"${this.updatedType.typeName}\"`
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            ticks: {
              callback: (tick: number) => {
                return this.convertEpochToSpecificTimezone(3, tick);
              }
            },
            scaleLabel: {
              labelString: 'Time',
              display: true,
            }
          }],
          yAxes: [{
            type: 'linear',
            ticks: {
              callback: (tick: number) => {
                return tick.toString();
              }
            },
            scaleLabel: {
              labelString: this.updatedType.code,
              display: true
            }
          }]
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.updatedData.firstChange) {
      this.refresh();
    }
  }

  removeDataSets() {
    this.chart.data.datasets = [];
    this.chart.update();
  }

  addDataSets(data: [string, SensorValue[]][]) {
    const dataSets = this.prepareDataSets(data);
    this.chart.data.datasets.push(...dataSets);
    this.chart.update();
  }

  convertEpochToSpecificTimezone(offset: number, epoch: number) {
    const d = new Date(epoch);
    const utc = d.getTime() + (d.getTimezoneOffset() * 60000);  // This converts to UTC 00:00
    const nd = new Date(utc + (3600000 * offset));
    return nd.toLocaleString();
  }
}
