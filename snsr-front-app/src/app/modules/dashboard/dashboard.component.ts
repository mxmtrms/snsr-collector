import {Component, OnInit} from '@angular/core';
import {BaseStationService} from '../../shared/services/base.station.service';
import {BaseStation} from '../../shared/models/base.station';
import {SensorService} from '../../shared/services/sensor.service';
import {Sensor} from '../../shared/models/sensor';
import {SensorValueType} from '../../shared/models/sensor.value.type';
import {SensorValue} from '../../shared/models/sensor.value';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  stations: BaseStation[];
  sensors: Sensor[];
  selectedStations: BaseStation[] = [];
  selectedSensors: Sensor[] = [];
  types: SensorValueType[] = [];
  selectedType: SensorValueType;
  showPlot = false;
  data: [string, SensorValue[]][] = [];
  type: SensorValueType;
  dateForm: FormGroup;
  minDate: string;
  maxDate: string;
  stationLoading: boolean;
  sensorLoading: boolean;
  typeLoading: boolean;

  constructor(private baseStationService: BaseStationService,
              private sensorService: SensorService,
              private formBuilder: FormBuilder) {
    this.dateForm = formBuilder.group({
      firstDate: new FormControl(),
      secondDate: new FormControl()
    });
  }

  ngOnInit(): void {
    this.stationLoading = true;
    this.baseStationService.getAllActiveBaseStations().subscribe((stations) => {
      this.stations = stations;
      this.stationLoading = false;
    });
    this.dateForm.valueChanges.subscribe((value) => {
      const t1 = this.dateForm.get('firstDate').value;
      const t2 = this.dateForm.get('secondDate').value;
      if (t1 !== null && t2 !== null && moment(t1).valueOf() <= moment(t2).valueOf()) {
        this.render();
      }
    });
  }

  // метод, который вызывается при изменении выбора станций
  stationSelectionChanged() {
    this.sensorLoading = true;
    this.sensors = [].concat(...this.selectedStations.map((station) => station.sensors)).filter(sensor => sensor.registered);
    this.sensorLoading = false;
  }

  sensorSelectionChanged() {
    if (this.selectedSensors.length !== 0) {
      this.typeLoading = true;
      this.sensorService.getCrossTypesBySensorIds(this.selectedSensors.map(sensor => sensor.id)).subscribe((types) => {
        if (this.selectedType === undefined) {
          if (this.types !== null) {
            this.types.splice(0, this.types.length);
            this.types.push(...types);
          } else {
            this.types = [].concat(...types);
          }
        }
        if (this.selectedType) {
          if (types.some(type => type.code === this.selectedType.code)) {
            const ind1 = types.findIndex((type) => type.code === this.selectedType.code);
            types.splice(ind1, 1);
            const ind2 = this.types.findIndex((type) => type.code === this.selectedType.code);
            this.types.splice(ind2 + 1, this.types.length);
            this.types.splice(0, ind2);
            this.types.push(...types.slice(ind2, types.length));
            this.types.unshift(...types.slice(0, ind2));
            this.typeSelectionChanged();
          } else {
            this.types.splice(0, this.types.length)
            this.types.push(...types);
          }
        }
        this.typeLoading = false;
      });
    } else {
      this.showPlot = false;
      this.types = null;
    }
  }

  typeSelectionChanged() {
    const dates = [] as number[];
    const sensorIds = this.selectedSensors.map((sensor) => sensor.id);
    if (this.selectedType) {
      this.sensorService.getValuesBySensorIdsAndTypeCode(sensorIds, this.selectedType.code).subscribe((map) => {
        for (const [key, values] of Object.entries(map)) {
          dates.push(...values.map(value => value.receivedTime));
        }
      }, () => {}, () => {
        this.minDate = moment(Math.min(...dates)).format('YYYY-MM-DD');
        this.maxDate = moment(Math.max(...dates)).format('YYYY-MM-DD');
        if (this.dateForm.get('firstDate').value != null && this.dateForm.get('firstDate').value != null) {
          if (this.dateForm.get('firstDate').value <= this.dateForm.get('secondDate').value) {
            this.render();
          }
        }
      });
    }
  }

  render() {
    const dataSets = [];
    if (this.selectedType) {
      const sensorIds = this.selectedSensors.map((sensor) => sensor.id);
      this.sensorService.getValuesBySensorIdsAndTypeCode(sensorIds, this.selectedType.code).subscribe((map) => {
        for (const [key, values] of Object.entries(map)) {
          const filteredValues = values.filter(value => {
            return value.receivedTime >= moment(this.dateForm.get('firstDate').value).valueOf() &&
              value.receivedTime <=  moment(this.dateForm.get('secondDate').value).add(1, 'days').valueOf() - 1;
          }) as SensorValue[];
          if (filteredValues.length !== 0) {
            dataSets.push([key, filteredValues]);
          }
        }
      }, () => {}, () => {
        if (dataSets.length === 0) {
          this.showPlot = false;
          this.dateForm.reset();
        } else {
          this.showPlot = true;
          this.data = dataSets;
          this.type = this.selectedType;
        }
      });
    }
  }
}
